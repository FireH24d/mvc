package services

import (
	"gitlab.com/FireH24d/mvc/domain"
	"gitlab.com/FireH24d/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
